#Disables ATS in NonSSL builds.
echo "Disables ATS in NonSSL build configrations"
echo "${CONFIGURATION}"
INFOPLIST="${TARGET_BUILD_DIR}"/"${INFOPLIST_PATH}"
echo "path == ${INFOPLIST}"
case "${CONFIGURATION}" in
"NonSSL")
/usr/libexec/PlistBuddy -c "Set :NSAppTransportSecurity:NSAllowsArbitraryLoads YES" "${INFOPLIST}"
;;
esac
