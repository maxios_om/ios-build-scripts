#!/bin/bash

set -e

# uniquify and sort the Xcode projct files
project_name=${PROJECT_NAME}
echo "project_name"
python $SRCROOT/Build-Phases/xUnique.py -u -s -v "${PROJECT_FILE_PATH}/project.pbxproj"
