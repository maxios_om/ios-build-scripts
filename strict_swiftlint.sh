#!/bin/sh
set -e

if ! which swiftlint > /dev/null; then
	echo "error: SwiftLint not installed, refer Using Homebrew section on https://github.com/realm/SwiftLint"
	exit 1
fi

swiftlint
